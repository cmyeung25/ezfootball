'use strict';

var express = require('express')
var cons = require('consolidate');
var app = express();
var bodyParser = require('body-parser');
var fs = require('fs');


app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

//middleware
app.use(function(req, res, next) {
  next();
})

app.engine('dust', cons.dust);

cons.dust.helpers = require('dustjs-helpers');
app.set('view engine', 'dust');
app.set('view cache', false);
app.set('views', __dirname + '/views');
app.use('/', express.static(__dirname + '/public'));

app.get('/:pageId', function(req, res) {
    var data = {};
    data.socialPanelItems = [1,2,3,4,5,6,7,8,9];
    res.render(req.params.pageId, data, function(err, out){
      if(!err) {
        fs.writeFile('public/' + req.params.pageId + '.html' , out )
      }
    });
    res.render(req.params.pageId, data);
});


module.exports = app;
if (!module.parent) {
  app.listen(process.env.PORT || 4888);
}
