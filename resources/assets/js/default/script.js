var jlib = function(){
  var self = this;
  var screenBreakpoint = {
    'screen_xs': 480,
    'screen_sm': 768,
    'screen_md': 992,
  };
  self.sideMenu = function(){
    $('.side-menu-toggle').on('click', function(e){
      $('body').toggleClass('side-menu-open');
    })
    $('.social-panel-toggle').on('click', function(e){
      $('body').toggleClass('social-panel-open');
      // self.categorySlickObj.slick('getSlick').resize()
      setTimeout(function(){
        self.categorySlickObj.slick('setPosition');
        $('.news-grids').masonry();
      },300);
      // self.categorySlickObj.resize();
      // self.productlistSlickObj.resize();

    })
    $('.social-panel-close').on('click', function(e){
      $('body').removeClass('social-panel-open');
    })

    $('.my-cart-toggle').on('click', function(e){
      $('body').toggleClass('my-cart-open');
    })

    $('.my-cart-close').on('click', function(e){
      $('body').removeClass('my-cart-open');
    })
    $(window).on('scroll', function(e){
      var $body = $('body');
      if($body.scrollTop() > 60) {
        $body.addClass('mobile-footer-open');
      } else {
        $body.removeClass('mobile-footer-open');
      }
    })

    if($(window).width() > 1740) {
      $('body').addClass('social-panel-open');
      setTimeout(function(){
        self.categorySlickObj.slick('setPosition');
      },300);
    }
  }
  self.dotdotdot = function(){
    $(window).resize(function(e){
      $('.card h3').dotdotdot();
      $('.footer-item-list h3').dotdotdot();
    })
    $('.card h3').dotdotdot();
    $('.footer-item-list h3').dotdotdot();
  }

  self.flyoutMenu = function() {
    //TODO FIYOUT EXIT RULE!


    var $headerFlyoutMenus = $('.header-flyout-menu');
    var $body = $('body');
    var flyoutExitWhitelistClasses = [
      '.header-flyout-menu',
      '.flyout-toggle-menu'
    ];
    $body.on('mouseover', function(e){
      if($body.hasClass('flyout-menu-shown')) {
          var $target = $(e.target);
          var exit = true;

          for (var i = 0; i < flyoutExitWhitelistClasses.length; i++) {
            if($target.closest(flyoutExitWhitelistClasses[i]).length > 0) {
              exit = false;
            }
          }

          if(exit) {
            $headerFlyoutMenus.removeClass('menu-show');
          }

      }
    })

    $('[data-toggle="flyout-menu"]').on('mouseover', function(e){
      var $this = $(this);
      var target = $this.attr('href');
      var $targetMenu = null;
      var $shownMenu = null;
      $headerFlyoutMenus.each(function(i,e){
        var $ele = $(e)
        if($ele.hasClass('menu-show')) {
          $shownMenu = $ele
        }
        if('#' + $ele.attr('id') == target) {
          $targetMenu = $ele;
        }
      })

      if($shownMenu != null && "#" + $shownMenu.attr('id') == target) {
        //do nothing
      } else {
        $headerFlyoutMenus.removeClass('menu-show');
        $targetMenu.addClass('menu-show');
        $body.addClass('flyout-menu-shown');
      }
    })
    $('.flyout-menu-nav [data-toggle="tab"]').on('mouseover', function(e){
      var $this = $(this);
      $($this).tab('show');
    })
  }
  self.categorySlick = function(){
    self.categorySlickObj = $('.category-slick').slick({
      dots: true,
      dotsClass:'category-slick-dots',
      // centerMode: true,
      // infinite: true,
      // centerPadding: '20px',
      slidesToShow: 3,
      slidesToScroll: 3,
      arrows: false,
    });
  }

  self.productlistSlick = function() {
    self.productlistSlickObj = $('.product-list-header-nav ul').slick({
      // variableWidth: true,
      slidesToScroll: 3,
      slidesToShow: 5,
      arrows: false,
      focusOnSelect: true,
    });
  }
  self.productlisting = function(){
    $('.card .card-thumbnail').each(function(){
      var $this = $(this);
      if($this[0].childNodes.length > 1) {
        var $productSlick = $this.slick({
          arrows: false,
          dots: true,
          dotsClass:'card-slick-dots',
          fade: true,
          speed: 100,
        });
        $('.card-slick-dots button').hover(function(){
          $(this).trigger('click');
        })
      }
    })
  }

  self.textField = function() {
    $('.form-group.text input[type="text"], .form-group.text input[type="password"]').on('change', function(e){
      var $this = $(this);
      if($this.val() == "") {
        $this.removeClass('filled')
      } else {
        $this.addClass('filled')
      }
    })
  }

  self.productrelatedSlick = function(){
    var slickConfig = {
      // variableWidth: true,
      slidesToScroll: 4,
      slidesToShow: 4,
      arrows: false,
      // focusOnSelect: true,
      responsive: [
        {
          breakpoint: screenBreakpoint.screen_md,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
          }
        },
        {
          breakpoint: screenBreakpoint.screen_sm,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
          }
        },
        {
          breakpoint: screenBreakpoint.screen_xs,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        },
      ]
    };

    $('#product-related-tabs').on('shown.bs.tab', function(e){
      // var $previousTab = $($(e.relatedTarget).attr('href'));
      // $previousTab.find('.productrelated-list').slick('unslick');
      var $targetTab = $($(e.target).attr('href'));
      // console.log($targetTab);
      var $productList = $targetTab.find('.productrelated-list');
      var $targetProductionList = $targetTab.find('.productrelated-list');
      if(!$targetProductionList.hasClass('slick-inited')) {
        $targetProductionList.slick(slickConfig);
        $targetProductionList.addClass('slick-inited');
      }
    })
    $('#product-related-tabs').on('hide.bs.tab', function(e){
      var $targetTab = $($(e.target).attr('href'));
      // console.log($targetTab);
      var $productList = $targetTab.find('.productrelated-list');
      var $targetProductionList = $targetTab.find('.productrelated-list');
      if($targetProductionList.hasClass('slick-inited')) {
        $targetProductionList.slick('unslick');
        $targetProductionList.removeClass('slick-inited');
      }
    })
    $('#product-related-tabs .tab-pane.active .productrelated-list').slick(slickConfig);
    $('#product-related-tabs .tab-pane.active .productrelated-list').addClass('slick-inited');
  }

  self.masonry = function() {
    $('.news-grids').masonry({
      itemSelector: '.news-item',
      columnWidth: '.news-sizer',
      horizontalOrder: true,
      percentPosition: true,
    });
  }

  self.priceFilterSlider = function() {
    $('#price-filter-mobile-slider').slider({
      range: true,
      min: 0,
      max: 1000,
      values: [0,1000],
      slide: function( event, ui ) {
        $(ui.handle).closest('.price-filter').find('.price-filter-slider-min').html(ui.values[ 0 ]);
        $(ui.handle).closest('.price-filter').find('.price-filter-slider-max').html(ui.values[ 1 ]);
      }
    });

    $('#price-filter-slider').slider({
      range: true,
      min: 0,
      max: 1000,
      values: [0,1000],
      slide: function( event, ui ) {
        $(ui.handle).closest('.price-filter').find('.price-filter-slider-min').html(ui.values[ 0 ]);
        $(ui.handle).closest('.price-filter').find('.price-filter-slider-max').html(ui.values[ 1 ]);
      }
    });
  }

  self.clubHome = function(){
    if(!$('#main').hasClass('club-home'))
        return


    $(window).on('scroll', function(e){
      var $body = $('body');

      if($body.scrollTop() > 150) {
        $('.secondary-header').addClass('header-show');
      } else {
        $('.secondary-header').removeClass('header-show');
      }
    })
  }

  self.slickAndZoom = function(){
    //product detail page
    //easyzoom
    // var $easyzoom = $('.easyzoom').easyZoom();
    // var easyZoomApi = $easyzoom.data('easyZoom');
    //
    // if(typeof easyZoomApi !== 'undefined') {
    //   easyZoomApi.opts.beforeShow = function() {
    //     return $('.easyzoom').hasClass('is-ready');
    //   }
    //
    //   $('.easyzoom').on('click', function(e){
    //     var $this = $(this);
    //     $this.toggleClass('is-ready');
    //
    //     if($('.easyzoom').hasClass('is-ready')) {
    //       easyZoomApi.show();
    //     } else {
    //       easyZoomApi.hide();
    //     }
    //   });
    // }

    //image-carousel
    var $slick = $('#image-carousel .image-carousel .image-carousel-list');
    $slick.slick({
      vertical: true,
      verticalSwiping: true,
      slidesToShow: 6,
      slidesToScroll: 2,
      infinite: false,
      prevArrow: '.image-carousel-control-prev',
      nextArrow: '.image-carousel-control-next',
    });

    $mainImageContainer = $('#main-image');

    $slick.on('click','.image-carousel-item', function(e){
      var $this = $(this);
      $this.closest('div').find('.image-carousel-item').removeClass('image-carousel-active-item');
      $this.addClass('image-carousel-active-item');

      var $img = $this.find('img');
      var imgUrl = $img.attr('data-image');
      var zoomUrl = $img.attr('data-zoom');
      // var tmpZoomStatus = $('.easyzoom').hasClass('is-ready');

      // $mainImageContainer.find('meta[name="twitter:image"]').attr('data-image', imgUrl);
      // easyZoomApi.swap(imgUrl,zoomUrl);
      $mainImageContainer.find('.productimagezoomable').attr('src', zoomUrl)

      // if(tmpZoomStatus) {
      //   $('.easyzoom').addClass('is-ready');
      // }
    })

    $mainImageContainer.on('dblclick', function(){
      handleZoom($(this));
    })

    $mainImageContainer.find('.productimagezoomable').panzoom({
      contain: 'invert',
      panOnlyWhenZoomed: true,
      transition: true,
      minScale: 1,
      increment: 1,
    });

    function handleZoom($ele) {
      var $imgContainer = $ele;
      if(!$imgContainer.hasClass('zoomed')) {
        $imgContainer.find('.productimagezoomable').panzoom('zoom');
      }else {
        $imgContainer.find('.productimagezoomable').panzoom('reset');
        $imgContainer.find('.productimagezoomable').panzoom('resetZoom');
      }
      $imgContainer.toggleClass('zoomed');
    }

    //mobile slick
    var $mobileSlick = $('#mobile-image-carousel > ul');
    $mobileSlick.slick({
      infinite: false,
      arrows: false,
      dots: true,
      dotsClass: 'mobile-image-slick-dots',
      lazyLoad: 'ondemand',
    });

    $mobileSlick.find('img').panzoom({
      contain: 'invert',
      panOnlyWhenZoomed: true,
      transition: true,
      minScale: 1,
      increment: 1,
    });

    $mobileSlick.find('li img').hammer().bind("doubletap", function(){
      handleMobileZoom($(this));
    });
    $mobileSlick.find('.zoom-icon').click(function() {
      handleMobileZoom($(this));
    })

    function handleMobileZoom($ele) {
      var $imgContainer = $ele.closest('li');
      if(!$imgContainer.hasClass('zoomed')) {
        $imgContainer.find('img').panzoom('zoom');
      }else {
        $imgContainer.find('img').panzoom('reset');
        $imgContainer.find('img').panzoom('resetZoom');
      }
      $imgContainer.toggleClass('zoomed');
    }
  }

  self.numberpicker = function(){
    $('input.number-picker').each(function(index, item){
      var $this = $(this);
      var $container;
      var inputValue ;
      var maxValue;
      var minValue;
      function init() {
        $this.addClass('form-control');
        inputValue = isNaN(parseInt($this.val())) ? 1 : parseInt($this.val());
        maxValue = isNaN(parseInt($this.attr('max'))) ? null : parseInt($this.attr('max'));
        minValue = isNaN(parseInt($this.attr('min'))) ? null : parseInt($this.attr('min'));

        $this.attr('value',inputValue);
        $this.after(' \
        <div class="input-group number-picker-container"> \
          <span class="input-group-btn number-reduce-btn">\
            <button class="btn btn-default" type="button"> - </button>\
          </span> \
        ' + $this[0].outerHTML + '\
          <span class="input-group-btn number-add-btn">\
            <button class="btn btn-default" type="button"> + </button>\
          </span>\
        </div>')
        $container = $this.siblings('.number-picker-container:not(.event-binded)');
        $container.addClass('event-binded');
        $this.detach();
      }

      function bindEvent() {
        $container.find('.number-reduce-btn').on('click', function(){
          if(minValue == null || inputValue > minValue) {
            inputValue--;
            updateValue();
          }
        })
        $container.find('.number-add-btn').on('click', function(){
          if(maxValue == null || maxValue > inputValue) {
            inputValue++;
            updateValue();
          }
        })

        var oldVal;
        var newVal;
        var $input = $container.find('.form-control');
        $input.on('keydown', function(e) {
          oldVal = $input.val();
        })
        $input.on('keyup', function(e) {
          newVal = $input.val();
          if(newVal > maxValue || newVal < minValue) {
            inputValue = oldVal;
          } else {
            inputValue = parseInt(newVal);
          }
          updateValue();
        })

      }

      function updateValue() {
        $container.find('.form-control').val(inputValue);
      }

      init();
      bindEvent();
    })
  }

  self.switchPanels = function(){
    $('.switch-panels .switch-panel').each(function(){
      var $panel = $(this);
      $panel.find('.switch-panel-content-collapse').collapse({
        toggle: false,
      });

      $panel.find('.switch-panel-toggle').bootstrapSwitch({
        size: 'small',
        onSwitchChange: function(event, state){
          var $this = $(this);
          if(state) {
            $($this.attr('data-target')).collapse('show');
          } else {
            $($this.attr('data-target')).collapse('hide');
          }
        }
      })
    })
    // $("").bootstrapSwitch({
    //   onSwitchChange: function(event, state){
    //     var $this = $(this);
    //   }
    // });
  }

  self.backToTop = function(){
    $('.back-to-top a').on('click',function(e){
      e.preventDefault();
      $('body').animate({
        scrollTop: "0px"
      });
    })
  }

  self.reviewSlider = function() {
    $('.review-slider-container').each(function(){
      var $this = $(this);
      $this.append('<h3>');
      $this.append('<div class="review-slider">');
      $this.append('<div class="review-slider-markers">');
      var rates = $this.attr('data-rates').split(',');
      var ratesDisplays = $this.attr('data-rates-display').split(',');
      var ratesCount = rates.length;
      var $markerContainers = $this.find('.review-slider-markers');

      for (var i = 0; i < ratesCount ; i++) {
        var str = ''
        if(ratesDisplays.find(function(e){return (e-1)==i})) {
          str = "<span>" + rates[i] + "</span>";
        }
        $markerContainers.append("<div style='left:" + i / (ratesCount - 1) * 100 + "%'>\
        " + str + "\
        </div>")
      }

      $this.find('.review-slider').slider({
        value: parseInt((ratesCount - 1)/2),
        min: 0,
        max: ratesCount - 1,
        step: 1,
        slide: function(event, ui){
          $this.find('h3').html(rates[ui.value]);
        }
      })

      $this.find('h3').html(rates[$this.find('.review-slider').slider('value')]);
    });
  }

  self.demo = function() {
    $('a[href="#"]').on('click', function(e){
      e.preventDefault();
    })
  }

  self.productDetail = function() {
    $('.add-to-basket-btn').on('click', function(){
      $('body').addClass('my-cart-open');
    })
  }

  self.sizeFilter = function() {
    var $secordSizeFilters = $('.second-size-filter');
    $('.top-size-filter a[data-toggle="tab"]').on('show.bs.tab', function(e){
      var $this = $(this);
      var tab = $this.attr('data-tab');

      $secordSizeFilters.removeClass("active");

      var $activeTab = $('.second-size-filter[data-parent-tab="' + tab + '"]')
      $activeTab.addClass("active");

      $activeTab.find('li').removeClass('active');
      $activeTab.find('a[href="' + $this.attr('href') + '"]').closest('li').addClass('active');


      // console.log(this);
      // console.log(e.target);
      // console.log(e.relatedTarget);
    })
  }

  self.unloadLoading = function(){
      window.onbeforeunload = function (e) {
          if ($('body').hasClass('isPersisted') == false) {
              $('body').addClass('unloading');
              $('a[href^=mailto]').each(function () {
                  setTimeout(function () {
                      $('body').removeClass('unloading');
                  }, 300);
              });
          }
          return undefined;
      }
    // window.addEventListener("pagehide", function (event) {
    //   $('body').addClass('unloading');
    // });
    //
    // $(document).on("pagehide",function(event){
    //   $('body').addClass('unloading');
    // })
    //
    // window.addEventListener("pageshow", function (event) {
    //   $('body').removeClass('unloading');
    // });
    //
    // $(document).on("pageshow",function(event){
    //   $('body').removeClass('unloading');
    // })

    // $(wind).on("pagehide",function(){
    //   alert('123123');
    //   $('body').addClass('unloading');
    // });
  }

  self.bootstrapSelect = function() {
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
      $('.selectpicker').selectpicker('mobile');
    }
  }

  self.checkoutDetail = function(){
    if($('.checkout-detail').length == 0)
      return

    var pageProgress = new progressIndicator();
    $('.address-prediction').addressPrediction();

    $('.process-payment').on('click', function(){
      //clear error
      $('.form-group').removeClass('error');

      // validation
      var data = $('#checkout-form').serializeObject();
      var validator = window.FormValidator;
      validator.setRules({
        'shipping_firstname': 'required',
      });
      validator.setData(data)

      validator.validate();

      if(!validator.getErrorBag()) {
        pageProgress.goToPage(2);
      } else {
        ///show error.
        var errors = validator.getErrorBag();
        $.each(errors, function(field, errors){
          $('[name="' + field +'"]').closest('.form-group').addClass('error');
          // console.log(e,i)
        })
      }
    })
  }

  showCheckoutMethodType($('[name="checkout_method"]:checked').val());

  $('[name="checkout_method"]').on('change', function(e){
    showCheckoutMethodType($(e.target).val());
  })

  function showCheckoutMethodType(tabName) {
    $('.checkout-method-tab').removeClass('active');
    $('.checkout-method-tab[data-tab-name="' + tabName + '"]').addClass('active');
  }
  return self
}()



$(document).ready(function(){
  jlib.sideMenu();
  jlib.flyoutMenu();
  jlib.categorySlick();
  jlib.productlistSlick();
  jlib.productlisting();
  jlib.productrelatedSlick();
  jlib.textField();
  jlib.masonry();
  jlib.clubHome();
  jlib.priceFilterSlider();
  jlib.slickAndZoom();
  jlib.numberpicker();
  jlib.switchPanels();
  jlib.backToTop();
  jlib.reviewSlider();
  jlib.productDetail();
  jlib.checkoutDetail();
  jlib.dotdotdot();
  jlib.unloadLoading();
  jlib.sizeFilter();
  jlib.bootstrapSelect();
  jlib.demo();
})
