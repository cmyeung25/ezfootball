var hoverTable = function(){
  var self = this;
  self.each(function(){
    var self = $(this);
    var $cells = self.find('tbody td');

    self.init = function(){
      self.attr('data-hover-table-init', true);
      self.createDOM();
      self.bindEvents();
    }

    self.createDOM = function(){
      self.append('<div class="hover-layer-row"></div>')
      self.append('<div class="hover-layer-column"></div>')
      self.append('<div class="hover-layer-cell"></div>')
      self.highlightRow = self.find('.hover-layer-row').hide();
      self.highlightCol = self.find('.hover-layer-column').hide();
      self.highlightCell = self.find('.hover-layer-cell').hide();
    }

    self.bindEvents = function(){
      $cells.on('mouseover', function(e){
        var $cell = $(this);

        self.highlightRow.show().css({
          "width": self.width() + 2 + 'px',
          "height": $cell[0].offsetHeight + 4 + 'px',
          "left": '0px',
          "top": $cell[0].offsetTop - 2 + 'px',
        });

        self.highlightCol.show().css({
          "width": $cell[0].offsetWidth + 'px',
          "height": self.height() + 2 + 'px',
          "left": $cell[0].offsetLeft + 'px',
          "top": '0px',
        });

        self.highlightCell.show().css({
          "width": $cell[0].offsetWidth - 4 + 'px',
          "height": $cell[0].offsetHeight + 'px',
          "left": $cell[0].offsetLeft + 2 + 'px',
          "top": $cell[0].offsetTop + 'px',
        });
        // window.$cell = $cell;

      })
      $cells.on('mouseout', function(e){
        var $cell = $(this);


        // self.unHighlightCell(row, column + 1)
      })

    }

    self.highlightCell = function(row, column){
      self.find('tr:nth(' + row + ')').addClass('highlight');
      self.find('td:nth-child(' + column + ')').addClass('highlight');
    }

    self.unHighlightCell = function(row, column){
      self.find('tr:nth(' + row + ')').removeClass('highlight');
      self.find('td:nth-child(' + column + ')').removeClass('highlight');
    }


    self.init();
  })
  return self;
};

$.fn.hoverTable = hoverTable;

$('.hover-table').hoverTable();
