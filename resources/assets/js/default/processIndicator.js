var progressIndicator = function (options) {
  var self = this;
  self.events = {};

  var config = $.extend({
    indicatorClass: 'progress-indicator',
    indicatorActiveClass: 'active',
    indicatorItemClass: 'progress-indicator-item',
    indicatorVisitedClass: 'visited',
    indicatorPageAttr: 'data-page',
    pageContinerClass: 'progress-indicator-pages',
    pageClass: 'progress-indicator-page',
    pageActiveClass: 'active',
    PagePageAttr: 'data-page',
    initPage: 1,
  },options);

  self.currentPage = 0;

  self.init = function() {
    self.indicator = $('.' + config.indicatorClass);
    self.pages = $('.' + config.pageContinerClass);

    self.goToPage(config.initPage);
  }

  self.goToPage = function(page) {
    self.indicator.find('.' + config.indicatorItemClass + '[' + config.indicatorPageAttr + '="' + self.currentPage + '"]').addClass(config.indicatorVisitedClass);

    self.indicator.find('.' + config.indicatorItemClass).removeClass(config.indicatorActiveClass)
    self.indicator.find('.' + config.indicatorItemClass + '[' + config.indicatorPageAttr + '="' + page + '"]').addClass(config.indicatorActiveClass);

    self.pages.find('.' + config.pageClass).removeClass(config.pageActiveClass)
    self.pages.find('.' + config.pageClass + '[' + config.PagePageAttr + '="' + page + '"]').addClass(config.pageActiveClass);

    self.trigger('change-page',[self,self.currentPage,page])
    self.currentPage = page;
  }

  self.on = function(event, callback) {
    if(typeof self.events[event] == 'undefined') {
      self.events[event] = [];
    }
    self.events[event].push(callback);
  }

  self.trigger = function(event, args) {
    if(typeof self.events[event] != 'undefined') {
      for (var i = 0; i < self.events[event].length; i++) {
        if(typeof args != 'underfined') {
          self.events[event][i](args[0],args[1],args[2],args[3],args[4])
        } else {
          self.events[event][i]();
        }
      }

    }
  }


  self.init()
  return self;
}
window.progressIndicator = progressIndicator;
