var addressPrediction = function(){
  //address prediction

  var AddressPredictionResult = [];
  var AddressPrediction = function(suggestedAddressModel,locale) {
    var self = this;
    self.locale = locale;
    self.districtDB = {
      en: {
        CW: 'Central and Western District',
        EST: 'Eastern District',
        ILD: 'Islands District',
        KLC :'Kowloon City District',
        KC :'Kwai Tsing District',
        KT :'Kwun Tong District',
        NTH :'North District',
        SK :'Sai Kung District',
        ST :'Sha Tin District',
        SSP :'Sham Shui Po District',
        STH :'Southern District',
        TP :'Tai Po District',
        TW :'Tsuen Wan District',
        TM :'Tuen Mun District',
        WC :'Wan Chai District',
        WTS :'Wong Tai Sin District',
        YTM :'Yau Tsim Mong District',
        YL :'Yuen Long District',
      },
      zh: {
        CW : '中西區',
        EST : '東區',
        ILD : '離島區',
        KLC : '九龍城區',
        KC : '葵青區',
        KT : '觀塘區',
        NTH : '北區',
        SK : '西貢區',
        ST : '沙田區',
        SSP : '深水埗區',
        STH : '南區',
        TP : '大埔區',
        TW : '荃灣區',
        TM : '屯門區',
        WC : '灣仔區',
        WTS : '黃大仙區',
        YTM : '油尖旺區',
        YL : '元朗區',
      }
    };
    self.data = {
      zh: {

      },
      en: {

      },
    };

    self.init = function(suggestedAddressModel) {
      self.retriveValues(suggestedAddressModel);
    }

    self.retriveValues = function(model,locale) {
      for(var i in model) {
        if(typeof model[i] == 'object') {
          if(i.startsWith("Chi")) {
            locale = 'zh';
          }
          if(i.startsWith("Eng")) {
            locale = 'en';
          }
          self.retriveValues(model[i], locale);
        } else {
          self.data[locale][i] = model[i];
        }
      }
    }

    self.getAttrValue = function(key, locale) {
      if(typeof locale == 'undefined') {
         locale = self.locale;
      }
      var spacing = '';
      switch (locale) {
        case 'en':
        spacing = ' ';
        break;
        case 'zh':
        spacing = '';
        break;
      }

      if(key == "BuildingNoTo") {
        return typeof self.data[locale][key] != 'undefined' ? '-' + spacing + self.data[locale][key] + spacing : '';
      } else {
        return typeof self.data[locale][key] != 'undefined' ? self.data[locale][key] + spacing : '';
      }
    }

    self.displayValue = function(locale) {
      if(typeof locale == 'undefined') {
         locale = self.locale;
      }

      returnValue = '';
      switch (locale) {
        case 'zh':
            returnValue = self.getAttrValue('Region',locale) + self.getDistrictName(self.getAttrValue('DcDistrict',locale),locale);

            if(self.getAttrValue('StreetName',locale) != '' ) {
              returnValue = returnValue + self.getAttrValue('StreetName',locale) + self.getAttrValue('BuildingNoFrom',locale) + self.getAttrValue('BuildingNoTo',locale);
            }

            if(self.getAttrValue('VillageName',locale) == '') {
                returnValue = returnValue + self.getAttrValue('EstateName', locale);
            } else {
                returnValue = returnValue + self.getAttrValue('VillageName', locale) + self.getAttrValue('BuildingNoFrom', locale) + self.getAttrValue('BuildingNoTo', locale);
            }

            returnValue = returnValue + self.getAttrValue('PhaseName', locale) + self.getAttrValue('BuildingName', locale) + self.getAttrValue('BlockNo', locale) + self.getAttrValue('BlockDescriptor', locale);
            break;
        case 'en':
            returnValue = self.getAttrValue('BlockDescriptor',locale) + self.getAttrValue('BlockNo',locale) + self.getAttrValue('BuildingName',locale);

            if(self.getAttrValue('VillageName',locale) == '') {
                returnValue = returnValue + self.getAttrValue('EstateName', locale);
            } else {
                returnValue = returnValue + self.getAttrValue('BuildingNoFrom', locale) + self.getAttrValue('BuildingNoTo', locale) + self.getAttrValue('VillageName', locale);
            }

            returnValue = returnValue + self.getAttrValue('PhaseName', locale);

            if(self.getAttrValue('StreetName',locale) != '' ) {
                returnValue = returnValue + self.getAttrValue('BuildingNoFrom', locale) + self.getAttrValue('BuildingNoTo', locale) + self.getAttrValue('StreetName', locale);
            }

            self.getDistrictName(self.getAttrValue('DcDistrict', locale), locale) + self.getAttrValue('Region', locale);
            break;
      }


      return returnValue;
    }

    self.getDistrictName = function(key, locale) {
        return typeof self.districtDB[locale][key] != 'undefined' ? self.districtDB[locale][key] : '';
    }

    self.getDistrictNameFull = function (key) {
        return typeof self.districtDB["en"][key] != 'undefined' ? key + ' ' + self.districtDB["en"][key] + ' ' + self.districtDB["zh"][key] : '';
    }

    self.init(suggestedAddressModel);
  }

  $.fn.addressPrediction = function(){
    var $return = [];
    $(this).each(function(){
      var $this = $(this);
      $this.attr('data-has-address',$this.hasClass('has-address'));
      var ignoreFocusOut = false;
      var $countryDropDown = $('[name="' + $this.attr('data-country-control-id') + '"]');
      var $predictionWrapper = $this.find('.address-prediction-wrapper');
      var $predictionInput = $predictionWrapper.find('.address-prediction-input');
      var $predictionResult = $predictionWrapper.find('.address-prediction-results');
      var $formAddressField = $('#' + $predictionWrapper.attr('data-address-field-id'));
      var $formAddressFloorField = $('#' + $predictionWrapper.attr('data-addressfloor-field-id'));
      var $formAddressFlatField = $('#' + $predictionWrapper.attr('data-addressflat-field-id'));
      var $formAddressDCDistrictField = $('#' + $predictionWrapper.attr('data-address-dcDistrict-field-id'));
      var $cantFoundBtn = $predictionWrapper.find('.address-prediction-cant-found');
      var $resultBuildingAddressLabel = $predictionWrapper.find('.result-building-address');
      var $resultBuildingAddressDCDistrictLabel = $predictionWrapper.find('.result-building-address-dcdistrict');
      var $searchBtn = $predictionWrapper.find('.address-predicion-search-button');
      var $hiddenLocaleField = $predictionWrapper.find('.address-prediction-hidden-locale input[type="hidden"]');
      var addressCantFindOption = $this.attr('data-address-cant-find-option');

      $this.excuteSearch = function() {
        // console.log($predictionInput);
        var value = $predictionInput.val();
        // console.log('search!', value);
        $predictionResult.html('');

        $predictionWrapper.removeClass('result-show');
        if(value.length < 2)
        return false;

        addressLookup(value, function (data) {
            console.log(data);
          if(/[^a-zA-Z0-9\s]/.test(data.RequestAddress.AddressLine[0])) {
              requestLocale = 'zh';
          } else {
              requestLocale = 'en';
          }
          $hiddenLocaleField.val(requestLocale);
          AddressPredictionResult = [];
          if(typeof data.SuggestedAddress != 'undefined') {
              for (var i = 0; i < data.SuggestedAddress.length; i++) {
                  if (i < 8) {
                      ap = new AddressPrediction(data.SuggestedAddress[i], requestLocale);
                      AddressPredictionResult.push(ap);
                  }
            }
          }
          renderAddressSelectOption($predictionResult, AddressPredictionResult, addressCantFindOption);

          $predictionWrapper.addClass('result-show');
          setTimeout(function () {
            $predictionInput.focus()
          }, 200);
        });
      }
      $this.handleCantFound = function() {
        $predictionWrapper.addClass('cant-found');
        $formAddressField.val('');
        $formAddressFlatField.val('');
        $formAddressFloorField.val('');
        $formAddressDCDistrictField.val('');
        $formAddressField.closest('.custom-address-field').show();
      }
      $this.initState = function() {
        if( $this.attr('data-has-address') == "true") {
            $this.addClass('has-address');
        } else {
            $this.handleChangeAddress(false);
        }
        //$formAddressField.val('');
        //$formAddressDCDistrictField.val('');
        $formAddressField.closest('.custom-address-field').hide();
      }

      $this.handleChangeAddress = function(isRestValue) {
          $predictionWrapper.removeClass('completed').removeClass('cant-found');
          if (isRestValue == true) {
              $formAddressField.val('');
              $formAddressFlatField.val('');
              $formAddressFloorField.val('');
              $formAddressDCDistrictField.val('');
          }
          $formAddressField.closest('.custom-address-field').hide();
          $this.removeClass('has-address');
          $this.removeClass('new-address');
      }

      $this.disable = function() {
          $this.handleChangeAddress(false);
          $predictionWrapper.addClass('cant-found');
          $formAddressField.closest('.custom-address-field').show();
      }

      $this.selectItem = function(ele){
        $selecteditem = $(ele);
        if($selecteditem.hasClass('address-prediction-cant-found')) {
            $this.handleCantFound();
        } else {
            console.log("dc", $selecteditem.attr("data-dcdistrict"));
            $formAddressDCDistrictField.val($selecteditem.attr("data-dcdistrict"));
            $formAddressField.val($selecteditem.html());
            $resultBuildingAddressLabel.html($selecteditem.html());
            $resultBuildingAddressDCDistrictLabel.html($selecteditem.attr("data-dcdistrict"));
            $predictionWrapper.addClass('completed');
            $this.addClass('new-address');
        }
        setTimeout(function () {
            ignoreFocusOut = false;
            $predictionResult.html('');
            $predictionInput.val('');
        });
      }

      $this.enable = function () {
          $this.initState();
      }

      $this.bindEvent = function(){
        if($predictionWrapper.hasClass('address-prediction-event-binded')) {
            return;
        }
        $predictionInput.on('keydown', function(e) {
          var $input = $(this);
          var currentSelectedIndex = typeof $input.attr('data-current-selected-index') == 'undefined' ? 0 : $input.attr('data-current-selected-index');
          var $predictionItems = $predictionWrapper.find('.prediction-item');
          $('.lblAddressMessage').show();
          switch(e.keyCode) {
              case 40:
                  e.preventDefault();
                  if (currentSelectedIndex >= $predictionItems.length || $predictionItems.length == 0) {
                      return;
                  }
                  $input.attr('data-arrow-select-active', 1);
                  currentSelectedIndex++;
                  $predictionWrapper.find('.prediction-item').removeClass('selected');
                  $targetOption = $predictionWrapper.find('.prediction-item:nth-child(' + currentSelectedIndex + ')');
                  $targetOption.addClass('selected');
                  calculateOptionVisible($targetOption);

                  $input.attr('data-current-selected-index', currentSelectedIndex);
                  break;
              case 38:
                  e.preventDefault();
                  if (currentSelectedIndex <= 1 || $predictionItems.length == 0) {
                      return;
                  }
                  $input.attr('data-arrow-select-active', 1);
                  currentSelectedIndex--;
                  $predictionWrapper.find('.prediction-item').removeClass('selected');
                  $targetOption = $predictionWrapper.find('.prediction-item:nth-child(' + currentSelectedIndex + ')');
                  $targetOption.addClass('selected');
                  calculateOptionVisible($targetOption);

                  $input.attr('data-current-selected-index', currentSelectedIndex);
                  break;
              case 13:
                  e.preventDefault();
                  if ($input.attr('data-arrow-select-active') == 1) {
                      var ele = $predictionWrapper.find('.prediction-item.selected');
                      $this.selectItem(ele);
                  } else {
                      $this.excuteSearch();
                      $input.attr('data-current-selected-index', 0);
                      $input.attr('data-arrow-select-active', 0);
                  }
                  return false;
                  break;
            default:
                $input.attr('data-arrow-select-active', 0);
                $predictionResult.html('');
                break;
          }
        });
        function calculateOptionVisible($ele) {
          //make sure the option is visible in the scorll list
            var wrapperHeight = $predictionResult.outerHeight();
            var wrapperScrollTop = $predictionResult.scrollTop();
            var wrapperOffsetTop = $predictionResult.offset().top;
            var eleHeight = $ele.outerHeight();
            var eleOffsetTop = $ele.offset().top;
            if(eleOffsetTop < wrapperOffsetTop){
                //overflow on top
                $predictionResult.scrollTop(eleOffsetTop - wrapperOffsetTop + $predictionResult.scrollTop());
            }
            if(eleOffsetTop + eleHeight > wrapperOffsetTop + wrapperHeight ){
                //overflow on bottom
                $predictionResult.scrollTop($predictionResult.scrollTop() + eleOffsetTop + eleHeight - wrapperOffsetTop - wrapperHeight);
            }
        }
        $searchBtn.on('click', function(e) {
          e.preventDefault();
          $this.excuteSearch();
          $('.lblAddressMessage').hide();
        });
        $predictionInput.on('focusout', function() {
          if(!ignoreFocusOut) {
            $predictionWrapper.addClass('focus-out');
          }
        })
        $predictionInput.on('focusin', function(){
          ignoreFocusOut = false;
          $predictionWrapper.removeClass('focus-out');
        })
        $predictionWrapper.on('mousedown', '.prediction-item', function() {
          ignoreFocusOut = true;
        })
        $predictionWrapper.on('click', '.prediction-item', function() {
          $this.selectItem(this);
        })
        $this.find('.change-address-trigger').on('click', function() {
            $this.handleChangeAddress(true)
            $('.lblAddressMessage').show();
        })

        $predictionWrapper.addClass('address-prediction-event-binded');
      }

      $this.bindEvent();

      //handle for country interaction
      function handleCountryToAddressPrediction(value) {
        if (value != 'HKG') {
            $this.disable()
        } else {
            $this.enable()
        }
      }
      // $countryDropDown.fancyfields("bind", "onSelectChange", function (input, text, value) {
      //     handleCountryToAddressPrediction(value);
      //     if (input.hasClass("customffEvent")) {
      //         var controlname = input.attr("name");
      //         setTimeout(__doPostBack(controlname, ""), 0);
      //     }
      // })
      
      handleCountryToAddressPrediction($countryDropDown.val())

      $return.push($this);
    })
    return $return;
  }

  // function bindAddressPredictionEvent() {
  $('.address-with-prediction').addressPrediction();
  // }
  // bindAddressPredictionEvent();
  if(typeof Sys != 'undefined') {
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_endRequest(function (s, e) {
        $('.address-with-prediction').addressPrediction();
    });
  }

  function debounce(func, wait, immediate) {
    var timeout;
    return function() {
      var context = this, args = arguments;
      var later = function() {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  };

  function addressLookup (text, callback) {
    if(text=='' || text.length < 1)
    {
      callback({
        SuggestedAddress: [],
      })
      return;
    }

    $.ajax({
      headers: {
        "Accept": "application/json",
        "Accept-Language": "*"
      },
      url: 'https://www.als.ogcio.gov.hk/lookup?q=' + text,
    })
    .then(function(data){
      callback(data);
    })
  }

  function renderAddressSelectOption($ele, predictionResultList, addressCantFindOption ) {

    if(typeof predictionResultList != 'undefined') {
        for (var i = 0; i < predictionResultList.length; i++) {
            console.log(predictionResultList[i]);
            var resultData = predictionResultList[i];
            $ele.append('<div class="prediction-item" data-DCDistrict="' + predictionResultList[i].getDistrictNameFull(resultData.data.en.DcDistrict) + '" data-address-en="' + predictionResultList[i].displayValue('en') + '"  data-address-zh="' + predictionResultList[i].displayValue('zh') + '">' + predictionResultList[i].displayValue() + '</div>');
        }
    }
    $ele.append('<div class="prediction-item address-prediction-cant-found">' + addressCantFindOption + '</div>')
  }
}()
