;(function($){
  FormValidatorClass = function() {
    //Usage: The validation class that allow user to define the form validation rule.
    //With assisted by ControlMapperClass, FormValidatorClass enable to write human readable rules
    //**Inspired by Laravel
    //https://laravel.com/docs/5.3/validation


    var self = this;
    self.rules = {};
    self.data = null;
    self.error = {};
    self.events = {};

    self.setRules = function(rules, ruleSet) {
      if(typeof ruleSet == 'undefined') {
        ruleSet = 'default';
      }
      self.rules[ruleSet] = rules;
      return self;
    }

    self.addRules = function (rules, ruleSet) {
        if (typeof ruleSet == 'undefined') {
            ruleSet = 'default';
        }
        $.extend(self.rules[ruleSet], rules);
        return self;
    }


    self.setData = function(data) {
      self.data = data
      return self;
    }

    self.getData = function(){
      return self.data;
    }

    //Main function to validate
    self.validate = function(ruleSet, data) {
      if(typeof ruleSet == 'undefined') {
        ruleSet = 'default';
      }

      if(typeof data != 'undefined') {
        self.data = data;
      }

      self.trigger('start-validate',[self, ruleSet])
      self.error = {};
      //loop for rules
      var rules = self.rules[ruleSet];

      for( key in rules ) {
        //get rule from collection
        var rulesToBeChecked = rules[key].split('|');
        for (var i = 0; i < rulesToBeChecked.length; i++) {
          validationFieldAgainstRule(key, rulesToBeChecked[i])
        }
      }

      //loop for conditional rules
      var conditionalRules = self.conditionalRules[ruleSet];
      if(typeof conditionalRules != 'undefined') {
        for (var i = 0; i < conditionalRules.length; i++) {
          if(conditionalRules[i].condition(self.data)) {
            var fields;
            var rules;

            if(typeof conditionalRules[i].fields != 'object') {
              fields = [conditionalRules[i].fields]
            } else {
              fields = conditionalRules[i].fields;
            }

            rules = conditionalRules[i].rules.split('|');


            for (var i = 0; i < fields.length; i++) {
              for (var j = 0; j < rules.length; j++) {
                validationFieldAgainstRule(fields[i], rules[j])
              }
            }
          }
        }
      }
      self.trigger('finish-validate',[self, ruleSet])

      if(self.getErrorBag() == false) {
        console.log('pass form validation for : ' + ruleSet);
        self.trigger('pass',[self]);
        return true;
      }

      self.trigger('error',[self, self.getErrorBag(), ruleSet]);
      return false;
    }

    function validationFieldAgainstRule (field, rule) {
      rulesToBeCheckedWithArgs = rule.split(':');
      validationRule = self.getRuleFromRuleCollection(rulesToBeCheckedWithArgs[0]);

      //argument that passing to the rule checking engine
      var args = {
        data: self.data,
        args: typeof rulesToBeCheckedWithArgs[1] == 'undefined' ? null : rulesToBeCheckedWithArgs[1],
      }
      if (validationRule != false) {
        if(!validationRule(self.data[field], args)) {
          if(typeof self.error[field] == 'undefined') {
            self.error[field] = [];
          }
          self.error[field].push(rule);
        }
      }
    }


    self.getErrorBag = function() {
      var returnValue = false;
      for (var key in self.error) {
        returnValue = self.error;
        break;
      }
      return returnValue;
    }

    self.getRuleFromRuleCollection = function(rule) {
      if(typeof self.ValidationRuleCollection[rule] != 'function') {
        console.warn('Error to impletement to the non function rule');
        return false;
      } else {
        return self.ValidationRuleCollection[rule];
      }
    }

    //event management
    self.on = function(event, callback, id) {
      if(typeof self.events[event] == 'undefined') {
        self.events[event] = [];
      }
      self.events[event].push({id:id, callback:callback});
    }

    self.trigger = function(event, args) {
      if(typeof self.events[event] != 'undefined') {
        for (var i = 0; i < self.events[event].length; i++) {
          if(typeof args != 'underfined') {
            self.events[event][i].callback(args[0],args[1],args[2],args[3],args[4])
          } else {
            self.events[event][i].callback();
          }
        }
      }
    }

    //Validation rule pool
    //Free to extend the validation rule from the following collection
    self.ValidationRuleCollection = {
      'required': function (value) {
        return typeof value != 'undefined' && $.trim(value) != '';
      },
      'required-terms' : function(value){
        return typeof value != 'undefined' && $.trim(value) != '';
      },
      'integer' : function (value){
        return value == parseInt(value, 10);
      },
      'isNoHtmlTag': function (value) {
          if ($.trim(value) == '') {
              return true;
          }
          //var regexp = /^<([a-z]+)([^<]+)*(?:>(.*)<\/\1>|\s+\/>)$/;
          var regexp = /<\/?[\w\s="/.':;#-\/\?]+>/gi;
          return (regexp.test($.trim(value)) == false);
      },
      'email': function(value) {
        if($.trim(value) == '') {
          return true;
        }
        var regexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regexp.test($.trim(value));
      },
      'tel' : function(value) {
        if($.trim(value) == '') {
          return true;
        }
        var regexp = /^[\s()+-]*([0-9][\s()+-]*){6,20}$/
        return regexp.test($.trim(value));
      },
      'passwordpattern' : function(value){
          if ($.trim(value) == '') {
              return true;
          }
          var regexp = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/
          return regexp.test($.trim(value));
      },
      'confirm': function(value, args) {
        var fulldata = args.data;
        var fieldToConfirm = args.args;
        return value == args.data[fieldToConfirm];
      }
    };

    //conditional validation
    //reference to laravel
    //https://laravel.com/docs/5.3/validation#conditionally-adding-rules
    self.conditionalRules = [];
    self.sometimes = function(fields, rules, condition, ruleSet){
      if(typeof ruleSet == 'undefined') {
        ruleSet = 'default';
      }
      // console.log('pushing conditional validation rules')
      if(typeof self.conditionalRules[ruleSet] == 'undefined') {
        self.conditionalRules[ruleSet] = [];
      }
      self.conditionalRules[ruleSet].push({
        'fields': fields,
        'rules': rules,
        'condition' : condition,
      })
    }

  }

  // set controlmapper into global scope
  window.FormValidator = new FormValidatorClass()
})($)
