process.env.DISABLE_NOTIFIER = false;
process.env.NODE_ENV = 'development';
var elixir = require('laravel-elixir');
require('laravel-elixir-livereload');

var bourbon = require('bourbon').includePaths;
var jquery = require('gulp-jquery');
var gulp = require('gulp');
var browserify = require('gulp-browserify');
var concat = require('gulp-concat');
var build = {};


// elixir.extend('amlBuild', function(){
//   new elixir.Task('amlBuild',function(){
//     return gulp.src(['resources/assets/js/aml/aml.js'])
//     .pipe(browserify({
//       insertGlobals : true,
//     }))
//     .pipe(concat('aml.js'))
//     .pipe(gulp.dest('public/js'))
//   })
// })

build.default = function(mix) {
  mix.sass([
    // '../../../node_modules/bootstrap/dist/css/bootstrap.css',
    'common/bootstrap.css',
    '../../../node_modules/jquery-ui-dist/jquery-ui.css',
    '../../../node_modules/wowjs/css/site.css',
    '../../../node_modules/wowjs/css/libs/animate.css',
    '../../../node_modules/slick-carousel/slick/slick.scss',
    '../../../node_modules/easyzoom/css/easyzoom.css',
    '../../../node_modules/bootstrap-select/dist/css/bootstrap-select.css',
    '../../../node_modules/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css',
    '../../../node_modules/flag-icon-css/css/flag-icon.css',
    'common/font-awesome.css',
    'default/style.scss'
  ],'public/css/default.css', {
      includePaths: bourbon,
      outputStyle: 'compact'
  });

  mix.scripts([
    '../../../node_modules/jquery/dist/jquery.js',
    '../../../node_modules/jquery-ui-dist/jquery-ui.js',
    '../../../node_modules/bootstrap/dist/js/bootstrap.js',
    '../../../node_modules/wowjs/dist/wow.js',
    '../../../node_modules/slick-carousel/slick/slick.js',
    '../../../node_modules/easyzoom/dist/easyzoom.js',
    '../../../node_modules/hammerjs/hammer.js',
    '../../../node_modules/jquery-hammerjs/jquery.hammer.js',
    '../../../node_modules/jquery.panzoom/dist/jquery.panzoom.js',
    '../../../node_modules/masonry-layout/dist/masonry.pkgd.min.js',
    '../../../node_modules/bootstrap-select/dist/js/bootstrap-select.js',
    '../../../node_modules/bootstrap-switch/dist/js/bootstrap-switch.js',
    '../../../node_modules/dotdotdot/src/js/jquery.dotdotdot.js',
    '../../../node_modules/form-serializer/dist/jquery.serialize-object.min.js',

    'common/*.js',
    'default/*.js',
  ],'public/js/default.js');
}


build.all = function(mix) {
  for(var i in this) {
    if(i != 'all') {
      this[i](mix)
    }
  }
}

elixir(function(mix) {
  var targetProject = process.env.project;
  if(typeof targetProject == 'undefined' || targetProject == 'all') {
    build.all(mix);
  } else {
    build[targetProject](mix);
  }
  mix.livereload();
});
